# pablorgarcia.gitlab.io/website/
_My personal website. A simple presentation card._

[pablorgarcia.gitlab.io](https://pablorgarcia.github.io/website/)


![License MIT](https://camo.githubusercontent.com/890acbdcb87868b382af9a4b1fac507b9659d9bf/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f6c6963656e73652d4d49542d626c75652e737667)

## Created with HTML5, CSS3, Vanilla JavaScript and 💛

### Features

- Faster as possible.
- Responsive web design, mobile first.
- All the content is flexible. To scale it only change the body font size `--font-size-base`.
- The layout styles changes depending on the hour of the day that is accessed. From the 8h until the 20h is rendered `at-day` class on body tag, for the other hours is rendered `at-night` class.
- To manage both layouts are using CSS native variables, `var(some-property)`, in the same way as SCSS or SASS.

### Performance

Google page speed test results:

[developers.google.com/pagespeed/pablorgarcia.gitlab.io](https://developers.google.com/speed/pagespeed/insights/?url=https%3A%2F%2Fpablorgarcia.gitlab.io/website/&tab=mobile)
- 99% Mobile
- 100% Descktop

If you want to have your own site version, feel free to fork it and go ahead! 🙂

Cheers!
